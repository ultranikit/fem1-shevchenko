const passwordInput = document.querySelector('#user-password');  // enter password input
const userPasswordIcon = document.querySelector('#user-icon-password');  // enter password icon

const passwordConfirm = document.querySelector('#confirm-password'); // confirm password input
const confirmPasswordIcon = document.querySelector('#confirm-icon-password'); // confirm password icon

let allIcons = document.querySelectorAll('.icon-password');
let allInputs = document.querySelectorAll('.input-password');

function changeIcon(chooseInput, chooseIconField) {
    let classShow = 'fa-eye';
    let classHide = 'fa-eye-slash';
    // проверим существует ли такой класс у иконки
    let checkClass = chooseIconField.classList.contains(classShow);

    // если да, назначим атрибуту type значение text - чтобы отобразить пароль
    // убирём класс иконки глаза и добавим класс иконки перечёркнутого глаза
    if (checkClass) {
        chooseInput.setAttribute('type', 'text');
        chooseIconField.classList.replace(classShow, classHide);
    } else {  // иначе сделаем тоже самое в другом порядке и назначим атрибуту type значение 'password' - чтобы скрыть пароль;
        chooseInput.setAttribute('type', 'password');
        chooseIconField.classList.replace(classHide, classShow);
    }
}

allIcons.forEach((item, index) => {
    item.addEventListener('click', () => {
        changeIcon(allInputs[index], item);
    });
});

// for (let i = 0; i < allIcons.length; i++) {
//     allIcons[i].addEventListener('click', () => {
//         changeIcon(allInputs[i], allIcons[i]);
//     });
// }

const confirmButton = document.querySelector('#confirm-button');
const checkValue = document.querySelector('#check-value');
confirmButton.addEventListener('click', () => {
    let firstValue = passwordInput.value;
    let secondValue = passwordConfirm.value;

    if (firstValue === secondValue && firstValue !== '' && secondValue !== '') {
        checkValue.innerText = '';
        setTimeout(() => {alert('You are welcome')}, 100);
    } else {
        checkValue.innerText = 'Нужно ввести одинаковые значения';
    }
});




// userPasswordIcon.addEventListener('click', () => {
//     changeIcon(passwordInput, userPasswordIcon);
// });
//
// confirmPasswordIcon.addEventListener('click', () => {
//     changeIcon(passwordConfirm, confirmPasswordIcon);
// });



// userPasswordIcon.addEventListener('click', () => {
//
//     // проверим существует ли такой класс у иконки
//     let checkClass = userPasswordIcon.classList.contains('fa-eye');
//     // если да, назначим атрибуту type значение text - чтобы отобразить пароль
//     // убирём класс иконки глаза и добавим класс иконки перечёркнутого глаза
//     if (checkClass){
//         passwordInput.setAttribute('type', 'text');
//         userPasswordIcon.classList.remove('fa-eye');
//         userPasswordIcon.classList.add('fa-eye-slash');
//
//     } else {  // иначе сделаем тоже самое в другом порядке и назначим атрибуту type значение 'password' - чтобы скрыть пароль;
//         passwordInput.setAttribute('type', 'password');
//         userPasswordIcon.classList.remove('fa-eye-slash');
//         userPasswordIcon.classList.add('fa-eye');
//     }
// });
//
// confirmPasswordIcon.addEventListener('click', () => {
//
//     let checkClass = confirmPasswordIcon.classList.contains('fa-eye');
//     if (checkClass){
//         passwordConfirm.setAttribute('type', 'text');
//         confirmPasswordIcon.classList.remove('fa-eye');
//         confirmPasswordIcon.classList.add('fa-eye-slash');
//
//     } else {
//         passwordConfirm.setAttribute('type', 'password');
//         confirmPasswordIcon.classList.remove('fa-eye-slash');
//         confirmPasswordIcon.classList.add('fa-eye');
//     }
// });