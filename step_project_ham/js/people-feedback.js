
let users = {
    0: {
        user_name: 'Liza Brown',
        user_work: 'Frontend developer',
        text: `Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.`, user_image: 'face1.png'
    },
    1: {
        user_name: 'Jack Pot',
        user_work: 'Backend developer',
        text: `Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.`, user_image: 'face2.png'},
    2: {
        user_name: 'Hasan Ali',
        user_work: 'Web Designer',
        text: `Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus,`, user_image: 'face3.png'},
    3: {
        user_name: 'Sarah Rafferty',
        user_work: 'Team Lead',
        text: `Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, nonTempus ultricies luctus,
                dignissim, augue tempus ultricies lepus dignissim, augue tempus ultricies l dignissim, augue tempus ultricies l`,
        user_image: 'face4.png'
    }
};

let usersListItem = $('.feedback-users-list li');
usersListItem.on('click', paginationChange);

buttonsSlide();

function clearUserInfo() {
    $('.feedback-user-message').remove();  // remove user comment
    $('.feedback-user-name').remove();     // remove user name
    $('.feedback-user-position').remove(); // remove user work position
    $('.feedback-user-image-container').remove(); // remove user image
}

// функция изминения слайда по нажатию на иконку пользователя
function paginationChange() {
    const dataValue = $(this).data('slide'); // получаем data attribute value из data-slide

    // удаляем класс активной вкладки и добавляем класс на нажатую вкладку
    $('.feedback-user-small-image-container.active-user').removeClass('active-user');
    $(this).addClass('active-user');

    // очистим инфу юзера
    clearUserInfo();
    // найдем нужный обьект юзера с информацией
    let user = findUser(users, dataValue);
    // передадим найденный обьект информации юзера в следущею функцию и покажем его
    showUser(user);
}

/* функция поиска нужного юзера
* принимает в себя обьект всех юзерови ключ полученый из data attribute */
function findUser(usersObject, dataAttrValue) {
    let user = {};
    for (let key in usersObject) {
        if (usersObject.hasOwnProperty(key)) {
            if (Number(key) === dataAttrValue) {
                return user = usersObject[key];
            }
        }
    }
}

// функция принимает в себя найденный обьект пользователя
function showUser(userObject) {

    // обозначу основные селекторы которые будут добавлять в себя нужные элементы
    let userCommentField = document.querySelector('.feedback-all-messages');
    let userInfoField = document.querySelector('.feedback-user-details');

    // создаем необходимые элементы
    const userComment = document.createElement('p');
    const userName = document.createElement('p');
    const userWork = document.createElement('p');
    const userImageField = document.createElement('div');
    const userImage = document.createElement('img');

    // задаем каждому элементу нужный класс
    userComment.classList.add('feedback-user-message');
    userName.classList.add('feedback-user-name');
    userWork.classList.add('feedback-user-position');
    userImageField.classList.add('feedback-user-image-container');
    userImage.classList.add('feedback-user-img');

    // записываем значения в элементы, которые мы получаем из найденного обьекта
    userComment.innerText = userObject.text;
    userName.innerText = userObject.user_name;
    userWork.innerText = userObject.user_work;
    userImage.setAttribute('src', `./img/people-feedback/${userObject.user_image}`);

    // спрячим все элементы
    userComment.hidden = true;
    userName.hidden = true;
    userWork.hidden = true;
    userImageField.hidden = true;

    // добавим элементы в нужный селектор
    userCommentField.appendChild(userComment);

    userInfoField.appendChild(userName);
    userInfoField.appendChild(userWork);

    userImageField.appendChild(userImage);
    userInfoField.appendChild(userImageField);

    // покажем все элементы с анимацией появления с помощью jquery fadeIn(duration)
    $(userComment).fadeIn(1000);
    $(userName).fadeIn(1000);
    $(userWork).fadeIn(1000);
    $(userImageField).fadeIn(1000);
}



function buttonsSlide() {
    let buttonPrev = $('.controller-prev');
    buttonPrev.on('click', slidePrev);
    let buttonNext = $('.controller-next');
    buttonNext.on('click', slideNext);
}


// функция для клавиши слайд назад
function slidePrev() {
    //выбираем иконку которая активна в данный момент
    let oldActiveUser = $('.feedback-user-small-image-container.active-user');
    // убираем класс active и добавляем предыдущему элементу
    oldActiveUser.removeClass('active-user')
        .prev().addClass('active-user');

    // очищаем значения пользователя (текст, имя, фотку)
    clearUserInfo();
    // возьмём значение data-attribute у активного пользователя
    let dataValue = $('.active-user').data('slide');

    // если значение undefined то это значит что предыдущего пользователя
    // не существует и нам надо начать с конца списка
    if (typeof dataValue === 'undefined') {
        // возьмём значение data-attribute у последнего пользователя
        dataValue = $('.feedback-user-small-image-container').length - 1;
        // получим последнеий элемент списка  и добавим ему класс active
        let newActiveUser = $('.feedback-users-list').children().last();
        newActiveUser.addClass('active-user');
        // найдем информацаю об пользователе и запишим её в перменную
        // передаем обьект со всеми пользователями и значение полученого атрибута
        // чтобы найти нужного пользователя
        let user = findUser(users, dataValue);
        // выведем информацю пользователя
        showUser(user);
    } else {
        let user = findUser(users, dataValue);
        showUser(user);
    }
}

// функция для клавиши слайд вперёд

function slideNext() {
    let oldActiveUser = $('.feedback-user-small-image-container.active-user');

    oldActiveUser.removeClass('active-user')
        .next().addClass('active-user');

    clearUserInfo();
    let dataValue = $('.active-user').data('slide');

    if (typeof dataValue === 'undefined') {
        dataValue = 0;
        let newActiveUser = $('.feedback-users-list').children().first();
        newActiveUser.addClass('active-user');
        let user = findUser(users, dataValue);
        showUser(user);
    }
    else {
        let user = findUser(users, dataValue);
        showUser(user);
    }
}