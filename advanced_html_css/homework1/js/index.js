let menuButton = document.querySelector('.navbar-menu-button');
let menuIcon = document.querySelector('#menu-icon');
let mobileMenu = document.querySelector('#mobile-menu');
let mobileTitle = document.querySelector('.navbar-mobile-title');
let desktopHeaderLogo = document.querySelector('.header-logo');


function resize() {
    if ( $(window).width() < 980) {
        $(desktopHeaderLogo).hide();
        $(mobileMenu).hide();

        $(menuButton).show();
        $(mobileTitle).show();
    }
    else {
        $(desktopHeaderLogo).show();
        $(mobileMenu).show();

        $(menuButton).hide();
        $(mobileTitle).hide();
    }
}

$(window).on("resize", resize);
resize(); // call once initially

document.addEventListener('click', (event) => {
    let isOpen = menuButton.classList.contains('opened');

    if (event.target !== menuButton && event.target !== menuIcon && isOpen) {
        menuButton.classList.remove('opened');
        changeMenuIcon('menu-icon');
        $(mobileMenu).slideToggle('slow');
    }
});

menuButton.addEventListener('click', (event) => {
    changeMenuIcon('menu-icon');

    if (menuButton.classList.contains('opened')) {
        menuButton.classList.remove('opened');
        $(mobileMenu).slideToggle('slow');
    }
    else if (event.currentTarget === menuButton) {
        menuButton.classList.add('opened');
        $(mobileMenu).slideToggle('slow');
    }
});

function changeMenuIcon(iconId) {
    const icon = document.querySelector(`#${iconId}`);

    if (icon.classList.contains('navbar-menu-icon')) {
        icon.className = 'fas fa-times navbar-menu-close-icon';
    } else {
        icon.className = 'fas fa-bars navbar-menu-icon';
    }
}