function createNewUser() {
    let name = prompt('Enter your name', 'name');
    let surName = prompt('Enter your surname', 'surname');
    let userDate = prompt('Enter your date of born in format dd.mm.yyyy', '22.05.2000');

    const newUser = {
        firstName: name,
        lastName: surName,
        birthday: userDate,

        getLogin: function() {
            let nameResult =  (this.firstName[0]+ this.lastName).toLocaleLowerCase();
            return nameResult;
        },
        getAge: function () {
            let currentDate = new Date();

            let changeDayPosition = this.birthday.split('.');
            let day = changeDayPosition[0];
            let month = changeDayPosition[1];
            changeDayPosition[0] = month;
            changeDayPosition[1] = day;
            let birthday = changeDayPosition.join('.');

            this.birthday = birthday;
            birthday = new Date(this.birthday);
            const currentMonth = currentDate.getMonth();
            const currentDay = currentDate.getDate();
            const birthMonth = birthday.getMonth();
            const birthDay = birthday.getDate();

            let age = currentDate.getFullYear() - birthday.getFullYear();

            if (currentMonth > birthMonth) {
                return age;
            } else if ( currentMonth === birthMonth && currentDay >= birthDay) {
                return age;
            } else {
                return age = age - 1;
            }

        },
        getPassword: function () {
            let getYear = new Date(this.birthday).getFullYear();
            let passwordResult = ( this.firstName[0].toUpperCase() + this.lastName.toLocaleLowerCase() +
                getYear );
            return passwordResult;
        }

    };

    return newUser;
}

const user = createNewUser();

console.log('user age is ' + user.getAge());
console.log('user - ' + user.getPassword());