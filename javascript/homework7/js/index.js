let firstArray = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv',['kek','new', 12]];
let secondArray = ['1', '2', '3', 'sea', 'user', 23];

createList(firstArray);
createList(secondArray);

function createList(array) {

    let fragment = document.createDocumentFragment();
    let list = document.createElement('ul');

    array.map(arrayItem => {

        if (typeof arrayItem === 'object'){
            let newList = document.createElement('ul');
            for (let k of arrayItem) {
                newList.innerHTML += `<li>${k}</li>`
            }
            list.insertBefore(newList, list.nextSibling);

        } else {
            let item = `<li>${arrayItem}</li>`;
            list.innerHTML += item;
        }
    });
    fragment.appendChild(list);
    document.body.appendChild(fragment);

}

