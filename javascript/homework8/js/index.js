const priceInput = document.querySelector('#priceInput');
const errorMessage = document.querySelector('#error-message');
const resultBlock = document.querySelector('#result');

priceInput.addEventListener('focus', () => {
    priceInput.classList.remove('error-border');
    priceInput.classList.add('add-focus');
    priceInput.style.color = 'black';

});

priceInput.addEventListener('blur', () => {
    priceInput.classList.remove('add-focus');
    if (Number(priceInput.value < 0)){
        errorMessage.innerText = 'Please enter correct value';
        errorMessage.style.color = 'red';
        priceInput.classList.add('error-border');
    } else if (priceInput.value !== ''){
        createPriceItem();
        priceInput.style.color = 'lightgreen';
        errorMessage.innerText = '';
    }
});


function createPriceItem() {
    const priceBlock = document.createElement('div') ;
    const priceValueSpan = document.createElement('span');
    const deleteBtn = document.createElement('button');

    deleteBtn.addEventListener('click', () => {
        priceBlock.remove();
        priceInput.value = '';
        errorMessage.innerText = '';
        priceInput.classList.remove('error-border');
    });

    priceBlock.classList.add('price-block');

    priceValueSpan.classList.add('decorate-span');
    priceValueSpan.innerText = `Текущая цена: ${priceInput.value}`;

    deleteBtn.classList.add('delete-btn');
    deleteBtn.innerText = 'x';

    priceValueSpan.appendChild(deleteBtn);
    priceBlock.appendChild(priceValueSpan);

    resultBlock.appendChild(priceBlock);
}

