let ourServiceTabsText = {
    'Web Design': {text: `Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`, image: 'web-design1.jpg'},

    'Graphic Design': {text: `Ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut `, image: 'web-design2.jpg'},

    'Online Support': {text: `Dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.`, image: 'web-design3.jpg'},

    'App Design': {text: `Sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna!`, image: 'web-design4.jpg'},

    'Online Marketing': {text: `Amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia?`, image: 'web-design5.jpg'},

    'Seo Service': {text: `Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit!!!`, image: 'web-design6.jpg'}
};

const ourServices = document.querySelector('.our-services-list');
// console.log(ourServices);
// let ourServicesItems = Array.from(ourServices.children);
// console.log(ourServicesItems);
ourServices.addEventListener('click', (event) => {
    let isActiveListItem = event.target;
    // console.log(isActiveListItem);
    let classImage = 'our-services-content-image';
    let classText = 'our-services-content-description';
    let listClass = 'our-services-list';
    changeTabsText(listClass, isActiveListItem, ourServiceTabsText, classImage, classText)
});


// передаем в функцию classUl - класс списка вкладок <ul class='list'>
// checkIsActive - получаем при нажатии на вкладку
// data - обьект с данными в виде data = { 'tab name': {text: `some-text`, image: 'web-design.jpg'} }
// classImage класс котоый при селекторе даст элемент img для изминения атрибута src='';
// classText класс который при селекторе даст элемент в который будет перезаписан текст например <p class='text'> my new </p>
function changeTabsText(listClass, checkIsActive, data, classImage, classText) {
    // получаем наш лист <ul>
    const selectedList = document.querySelector(`.${listClass}`);
    // делаем нод листа массив ;
    let arrayTabs = Array.from(selectedList.children);
    // присваиваем строку с классами в checkIsList
    let checkIsList = selectedList.className;
    //проверяем если нажата влкадка у которой уже есть класс 'active' то вернём тру
    if (checkIsActive.classList.contains('active')) {
        return true;
        // если если нажатая влкадка оказалась Листом <ul> то вернём фолс
    } else if (checkIsActive.classList.contains(checkIsList)){
        return false;
    } else {
        // в любом другом случае должна быть нажата именно вкладка
        // найдем активную вкалдку из массива элементов <li> у которой есть class="active';
        let findActive = arrayTabs.find(item => item.classList.contains('active'));
        // удалим класс 'active'
        findActive.classList.remove('active');

        // у нажатой влкадки возьмём текст и переведем его в нижний регистр и запишим в tabName
        let tabName = checkIsActive.innerText.toLowerCase();
        // теперь с помозью Object.keys() получим  массив включей нашего обьекта который мы передали
        // сравним ключи которые есть в обьекте с названием вкладки и если таков есть запишим ключ в переменную
        let findEqualKeys = Object.keys(data).find(key => key.toLowerCase() === tabName);
        // выделим элементы в которые будем перезаписывать значение текст и картинку
        let tabContentText = document.querySelector(`.${classText}`);
        let tabContentImage = document.querySelector(`.${classImage}`);

        // Добавим класс 'active' новой вкладке
        checkIsActive.classList.toggle('active');
        // из findEqualKeys мы получили ключ по которому лежит еще один обьект который хранит 2 ключа
        // ключ text и image у которых свои значение , запишим найденный обект в переменную
        let newImageText = data[findEqualKeys];
        // запишим текст в эемент с классом который мы передавали как аргумент и то же самое с image
        tabContentText.innerText = newImageText.text.replace(/\r?\n/g, '');
        tabContentImage.setAttribute('src', `./img/our-services/${newImageText.image}`);
    }
}

