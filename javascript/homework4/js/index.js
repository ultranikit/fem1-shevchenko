function createNewUser() {
    let name = prompt('Enter your name');
    let surName = prompt('Enter your surname');

    const newUser = {
        firstName: name,
        lastName: surName,
        getLogin: function() {
            let result =  (this.firstName[0] + this.lastName).toLocaleLowerCase();
            return result;
        }
    };
    return newUser;
}

const user = createNewUser();
console.log('user - ' + user.getLogin());