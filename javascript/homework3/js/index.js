let firstNumber = Number(prompt('Enter first number'));
let secondNumber = Number(prompt('Enter second number'));
let operationInput = prompt('Enter operation');

validation(firstNumber, secondNumber, operationInput);


function calculate(firstNum, secondNum, operation) {

    let result = 0;

    switch (operation) {
        case '+':
            result = firstNum + secondNum;
            break;

        case '-':
            result = firstNum - secondNum;
            break;

        case '*':
            result = firstNum * secondNum;
            break;

        case '/':
            result = firstNum / secondNum;
            break;
    }
    return result;
}

function validation(firstNum, secondNum, operation) {

    // делаем проверку что введены именно числа
    while(isNaN(firstNum) || isNaN(secondNum)) {
        firstNum = Number(prompt('Enter first number', firstNum));
        secondNum = Number(prompt('Enter first number', secondNum));
        operation = prompt('Enter operation', operation);
    }

    if(operation === '+' || operation === '-' || operation === '*' || operation === '/') {
        console.log(calculate(firstNum, secondNum, operation));
    } else {
        let operation = prompt('Enter operation', 'Operation was wrong, try again');
        return validation(firstNum, secondNum, operation);
    }

}


