$('#slideToggleBtn').click(function () {
    $('.popular-posts').slideToggle('slow');
});

let backToTop = $('#back-to-top');
backToTop.hide();

backToTop.on('click', function () {
    $("body,html").animate({scrollTop: 0}, 1000);
    backToTop.fadeOut(500);
    return false;
});

$(".navbar-menu").on("click","a", function () {
    // забираем id бока с атрибута href
    let id  = $(this).attr('href');
    // //узнаем высоту от начала страницы до блока на который ссылается якорь
    let top = $(id).offset().top - 50;
    if (top > 320) {
        backToTop.fadeIn(500);
    } else {
        backToTop.fadeOut(500);
    }
    // //анимируем переход
    $('body,html').animate({scrollTop: top}, 1000);
});






