// let numberRange = Number(prompt('Enter number'));
// while (!parseInt(numberRange))  {
//     numberRange = Number(prompt('Enter number'));
// }
// if (numberRange >= 5) {
//     for (let i = 0; i <= numberRange; i++) {
//         if (i !== 0 && i % 5 === 0) {
//             console.log(i)
//         }
//     }
// } else {
//     console.log('Sorry, no numbers');
// }


let m = Number(prompt('Enter first number')),  //range start
    n = Number(prompt('Enter second number')); //range end

while (!Number.parseInt(m) || !Number.parseInt(n)) {
    alert('Fail, enter numbers again!');
    m = Number(prompt('Enter first number'));
    n = Number(prompt('Enter second number'));
}

for (let i = m; i <= n; i++) {
    if (primeCheck(i)) {
        console.log(i);
    };
}

function primeCheck(number) {
    if (number === 2) { // если число = 2, это простое число, выведем его в консоль
        return true;
    } else {  // елси два верхних условия не верны попадаем сюда, число больше 2
        for (let i = 2; i < number; i++) {
            // если число хоть раз поделится на цело до того как дойдет до самого себя, то оно не простое
            if (number % i === 0) {
                return false;
            }
        }
 /* иначе если число не делилось нацело из цикла выше,
  то оно делится только на себя - это простое число, мы его выводим */
        return true;
    }
}
