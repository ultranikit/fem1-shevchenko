let getButtons = document.querySelectorAll('.btn-wrapper button'); // получаем нод лист из кнопок
getButtons = Array.from(getButtons); // делаем из нод листа массив

pressKeys(getButtons);


function pressKeys(array) {
    document.addEventListener('keyup', (event) => { // добавляем на документ событие "keyup"
        let key = event.key;  // записываем в переменную key значение event.key
        // console.log(key);

        /* так как getButtons - массив, то можем использовать метод array.find() чтобы найти нужный элемент
        *  в момент keyup если значение innerText внутри кнопки будет равняться значению переменной key
        *  то нам вернется html элемент button, иначе елси такоого нету вернется undefined */
        let checkKey = array.find(item => item.innerText.toLocaleLowerCase() === key.toLocaleLowerCase());
        // console.log(checkKey);

        if (checkKey) {
            // елси такой элемент есть, то нам нужно очистить предыдущеий цвет кнопки.
            /* при первом нажатии мы не можем выбрать элемент с классом btn-bg-color так как его еще не существует
               и надо сделать проверку на null */
            const blueBtn = document.querySelector('.btn-bg-color');
            // console.log(blueBtn);
            if (blueBtn !== null) {
                // убераем предыдущему элементу класс цвет кнопки
                blueBtn.classList.remove('btn-bg-color');
            }
            // добавим класс цвета кнопки для нажатой клавиши
            checkKey.classList.add('btn-bg-color');
        } else if (checkKey === undefined) { // если значение undefined то просто выведем что-нибудь в консоль)
            console.log('Такой клавиши нету на странице');
        }
    });
}
