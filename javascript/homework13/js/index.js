const passwordInput = document.querySelector('#user-password');  // enter password input
const passwordConfirm = document.querySelector('#confirm-password'); // confirm password input


let allIcons = document.querySelectorAll('.icon-password');
let allInputs = document.querySelectorAll('.input-password');


// homework13 begin
const themeBtn = document.querySelector('.change-theme'); // кнопка смены темы
let allBtns = document.querySelectorAll('button'); // берем нд лист всех кнопок на странице

let storage = localStorage; //  local storage

// если в localStorage существует ключ 'dark' то выполнятся все действияя внутри if
if (storage.getItem('dark')) {
    // по ключу 'dark' лежит значение в виде строки
    // чтобыы получить обьект воспользуемся JSON.parse() и запишим его в перменную dark;
    let dark = JSON.parse(storage.getItem('dark'));

    // теперь мы можем получить значения ключей обьекта dark
    allBtns.forEach(btn => btn.classList.add(dark.buttons));
    document.body.classList.add(dark.body);
}

//при перезагрузке страници запишим все значения в обьект
// обьект переведем в строку с помощью JSON.stringify();
// и запишим нашу строку как значение по ключу 'input_values' с помощью storage.setItem();
document.addEventListener('keydown', (event) => {
    if (event.key === 'F5') {
        let inputValue = {};
        allInputs.forEach((item, index) => {
            inputValue[index] = item.value;
        });
        inputValue = JSON.stringify(inputValue);
        storage.setItem('input_values', inputValue);
    }
});

if (storage.getItem('input_values')) {
    // при записи в local storage мы превращаем обьект в строку
// JSON.parse вернет нам из строки обьект и этот обьект мы запишим в перменную allValues
    let allValues = JSON.parse(storage.getItem('input_values'));
// каждому полю Input присвоим значение из обьекта allValues по индексу (ключи обьекта === индексу элемента)
    allInputs.forEach((item, index) => item.value = allValues[index]);
}

// listener для кнопки change theme
themeBtn.addEventListener('click', () => {
    if (!storage.getItem('dark')) {
        // запишим обьект строкой в перменную pageChanges
        let pageChanges = JSON.stringify({body: 'dark-theme', buttons: 'buttons-dark'});
        // добавим ключ и значение в local storage
        storage.setItem('dark', pageChanges);

        // всем кнопкам добавим класс 'buttons-dark'
        allBtns.forEach(btn => btn.classList.add('buttons-dark'));
        //body добавим класс 'dark-theme';
        document.body.classList.add('dark-theme');
    } else {
        // вернём прежнюю тему
        //удалим ключ 'dark'
        storage.removeItem('dark');
        // удалим класс у всех кнопок
        allBtns.forEach(btn => btn.classList.remove('buttons-dark'));
        // удалим класс у body
        document.body.classList.remove('dark-theme');
    }
});
// homework13 end


function changeIcon(chooseInput, chooseIconField) {
    let classShow = 'fa-eye';
    let classHide = 'fa-eye-slash';
    // проверим существует ли такой класс у иконки
    let checkClass = chooseIconField.classList.contains(classShow);

    // если да, назначим атрибуту type значение text - чтобы отобразить пароль
    // убирём класс иконки глаза и добавим класс иконки перечёркнутого глаза
    if (checkClass) {
        chooseInput.setAttribute('type', 'text');
        chooseIconField.classList.replace(classShow, classHide);
    } else {  // иначе сделаем тоже самое в другом порядке и назначим атрибуту type значение 'password' - чтобы скрыть пароль;
        chooseInput.setAttribute('type', 'password');
        chooseIconField.classList.replace(classHide, classShow);
    }
}

allIcons.forEach((item, index) => {
    item.addEventListener('click', () => {
        changeIcon(allInputs[index], item);
    });
});

const confirmButton = document.querySelector('#confirm-button');
const checkValue = document.querySelector('#check-value');

confirmButton.addEventListener('click', (event) => {
    event.preventDefault();
    let firstValue = passwordInput.value;
    let secondValue = passwordConfirm.value;

    if (firstValue === secondValue && firstValue !== '' && secondValue !== '') {
        checkValue.innerText = '';
        setTimeout(() => {alert('You are welcome')}, 100);
    } else {
        checkValue.innerText = 'Нужно ввести одинаковые значения';
    }
});


